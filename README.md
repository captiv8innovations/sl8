sl8 (slate)
===================
sl8 is a customizable notification generator that displays a clean and elegant time-delayed HUD on the parent page.

## Terms of Use
sl8 is created and maintained by [captiv8 Innovations Inc](https://captiv8innovations.com). The code is available under the [captiv8 Terms of Use License](https://captiv8innovations.com/terms).