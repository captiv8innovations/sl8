(function( $ ){
    $.fn.sl8 = function(arr){
        var _this = this;
        $(_this).html("").css({position: 'relative', height: arr[0].height, overflow: 'hidden'});
        $.each( arr, function(){
            var obj = this;
            $(_this).append(
                $("<section/>", {id: obj.name}).css({position: 'absolute'}).append(
                    $('<div/>', {'class': 'text-block'})
                    .append(
                        $('<div/>', {'class': 'heading', text: obj.statement})
                    )
                    .append(
                        $('<div/>', {'class': 'subheading', text: obj.tagline})
                    )
                    .append(
                        function(){
                            var buttons = $('<div/>');
                            $.each(obj.buttons, function(){
                                var button = this;
                                //console.log($(_this).children("div.text-block#"+obj.name));
                                $(buttons).append($('<button/>', {text: button.name}));
                            });
                            return $(buttons);
                        }
                    )
                ).css(
                    {
                        backgroundImage: "url("+obj.backgroundImage + ")",
                        backgroundPosition: "center top",
                        backgroundSize: "cover",
                        height: obj.height + "px"
                    }).hide()
            );
        });
        var children = $(_this).children("section");
        $(children[0]).show();
        var count = 0;
        var current = setInterval(function(){
            count++;
            $(children).not(children[count-1]).css({left: $(_this).width()});
            if (count - 1 >= 0) {
                $(children[count-1]).animate({left: (-1 * $(_this).width())}, "slow",'easeInOutCirc');
            }
            if (count == arr.length ) {
                count = 0;
            }

            $(children[count]).css({left: $(_this).width()});
            $(children[count]).show().animate({left: 0}, "slow",'easeInOutCirc');

        }, arr[count].duration);
    };
}(jQuery));